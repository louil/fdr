#! /usr/bin/env python3

import os
import socket
import sys
import threading
import math

#Class idea found at http://www.tutorialspoint.com/python3/python_multithreading.htm 
#and functions derived from paradis
class myThread(threading.Thread):
	def __init__(self, host, port):
		threading.Thread.__init__(self)
		self.host = host
		self.port = port
		self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) 
		self.server_socket.bind((self.host, self.port))
		self.server_socket.setblocking(False)
		self.flag = 1
	
	#Function that will set the flag to 0
	def quitter(self):
		self.flag = 0
		
	#Function that contains the running body that grabs data from the client
	#and then will call any of the three main functions depending on the data
	def run(self):
			while(self.flag):
				try:
					self.data, addr = self.server_socket.recvfrom(1024)
					self.data = self.data.decode('utf-8')
					if self.data[0] == 'F':
						if int(self.data[1:]) < 0 or int(self.data[1:]) > 300:
							answer = "Numbers less than 0 or larger than 300 are not suppoted!"
							self.server_socket.sendto(bytes(answer, 'utf-8'), addr)
							continue
						answer = Fnumber(int(self.data[1:]))
						answer += '\n\0'
						self.server_socket.sendto(bytes(answer, 'utf-8'), addr)
					elif self.data[0] == 'D':
						if int(self.data[1:]) < 0 or int(self.data[1:]) > pow(10, 30):
							answer = "Numbers less than 0 or larger than 10^30 are not suppoted!"
							self.server_socket.sendto(bytes(answer, 'utf-8'), addr)
							continue
						answer = Dnumber(int(self.data[1:]))
						answer += '\n\0'
						self.server_socket.sendto(bytes(answer, 'utf-8'), addr)
					elif self.data[0] == 'R':
						answer = Rnumber(str(self.data[1:]))
						if answer == 0:
							answer = "Numbers less than I or larger than MMMM are not suppoted!"
						answer += '\n\0'
						self.server_socket.sendto(bytes(answer, 'utf-8'), addr)
				except BlockingIOError:
					"""stuff"""	
			self.server_socket.close()

#Found at https://technobeans.wordpress.com/2012/04/16/5-ways-of-fibonacci-in-python/ using the first example listed
def Fnumber(n):
	a,b = 1,1
	for i in range(n-1):
		a,b = b,a+b
	return hex(a)

#Function that returns the hex of the number provided
def Dnumber(number):
	return hex(number)

#Found at http://stackoverflow.com/questions/19308177/converting-roman-numerals-to-integers-in-python using the fourth example listed
def Rnumber(number):
	numerals = [
        {'letter': 'M', 'value': 1000},
        {'letter': 'D', 'value': 500},
        {'letter': 'C', 'value': 100},
        {'letter': 'L', 'value': 50},
        {'letter': 'X', 'value': 10},
        {'letter': 'V', 'value': 5},
        {'letter': 'I', 'value': 1},
    ]

	index_by_letter = {}

	for index in range(len(numerals)):
		index_by_letter[numerals[index]['letter']] = index

	result = 0
	previous_value = None
	for letter in reversed(number):
		try:
			index = index_by_letter[letter]
		except KeyError:
			return 0

		value = numerals[index]['value']
		if (previous_value is None) or (previous_value <= value):
		    result += value
		else:
		    result -= value
		previous_value = value

	if result < 1 or result > 4000:
		return 0

	return hex(result)

def main():
	#Get the uid
	user_id = os.getuid()		

	#Create my three ports as well as the host
	host = "127.0.0.1"
	port1 = user_id
	port2 = user_id + 1000
	port3 = user_id + 2000

	#Set up all three using threads
	myServer1 = myThread(host, port1)
	myServer2 = myThread(host, port2)
	myServer3 = myThread(host, port3)

	#Start all three
	myServer1.start()
	myServer2.start()
	myServer3.start()

	#Allows the server to accept both a user input method to quit
	#or to just use control c
	while True:
		try: 
			x = input("Type 'exit' to quit: ")
			if x == 'exit':
				myServer1.quitter()
				myServer2.quitter()
				myServer3.quitter()
				myServer1.join(timeout=1)
				myServer2.join(timeout=1)
				myServer3.join(timeout=1)
				break
		except KeyboardInterrupt:
			myServer1.quitter()
			myServer2.quitter()
			myServer3.quitter()
			myServer1.join(timeout=1)
			myServer2.join(timeout=1)
			myServer3.join(timeout=1)
			print()
			break
			
if __name__ == "__main__":
	main()






